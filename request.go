package worker

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"

	"gitlab.com/sensorbucket/go-worker/pipeline"
)

// apiResponse ...
type apiResponse struct {
	Status int                `json:"status,omitempty"`
	Data   []*pipeline.Device `json:"data,omitempty"`
}

// DeviceFilter is used to filter for a specific device
type DeviceFilter struct {
	Source map[string]interface{} `json:"source,omitempty"`
	Device map[string]interface{} `json:"device,omitempty"`
	Extra  map[string]string      `json:"-"`
}

// FindDevice finds a device
func (w *Worker) FindDevice(filter *DeviceFilter, authToken string) (*pipeline.Device, error) {
	query := url.Values{}

	// Add filter to the query parameters
	for k, v := range filter.Extra {
		query.Set(k, v)
	}

	// Add configuration filter
	confFilter, err := json.Marshal(filter)
	if err != nil {
		return nil, err
	}
	query.Set("configuration", string(confFilter))

	// Create request
	reqURI := fmt.Sprintf("%s/devices?%s", w.mgmtURI, query.Encode())
	req, err := http.NewRequest("GET", reqURI, nil)
	if err != nil {
		return nil, err
	}

	// Add authorization token (JWT)
	req.Header.Add("authorization", "bearer "+authToken)

	// Perform request
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	// Unmarshal responses to devicelist
	var resp apiResponse
	err = json.NewDecoder(res.Body).Decode(&resp)
	if err != nil {
		return nil, err
	}

	// Sanity check, should only return 1 value
	if len(resp.Data) == 0 {
		return nil, errors.New("No device found for uplink message")
	} else if len(resp.Data) > 1 {
		return nil, errors.New("Too many devices found for uplink message")
	}

	return resp.Data[0], nil
}
