package pipeline

import "time"

// MessageDirection indicates whether the message is an uplink or downlink message
type MessageDirection string

const (
	// UplinkMessage indicates the message is coming from the device
	UplinkMessage MessageDirection = "uplink"

	// DownlinkMessage indicates the message is going to the device
	DownlinkMessage MessageDirection = "downlink"
)

// MessageType indicates the contents of the message
type MessageType string

const (
	// SourceMessage the node message contains source data
	SourceMessage MessageType = "source"
	// DeviceMessage the node message contains device data
	DeviceMessage MessageType = "device"
	// MeasurementMessage the node message contains measurement data
	MeasurementMessage MessageType = "measurement"
)

// Device ...
type Device struct {
	ID            int `json:"id,omitempty"`
	LocationID    int `json:"location_id"`
	Configuration struct {
		Source map[string]interface{} `json:"source,omitempty"`
		Type   map[string]interface{} `json:"type,omitempty"`
	} `json:"configuration,omitempty"`
}

// NodeMessage represents a device message
type NodeMessage struct {
	ID         string           `json:"id,omitempty"`
	Direction  MessageDirection `json:"direction,omitempty"`
	Type       MessageType      `json:"type,omitempty"`
	DateTime   time.Time        `json:"date_time,omitempty"`
	Source     string           `json:"source,omitempty"`
	DeviceType string           `json:"device_type,omitempty"`
	Device     *Device          `json:"device,omitempty"`
	Owner      int              `json:"owner,omitempty"`
	Token      string           `json:"token"`
	Payload    interface{}      `json:"payload,omitempty"`
}

// DevicePayload represents the message that is going to the device worker
type DevicePayload struct {
	DeviceData string `json:"device_data"`
}

// RoutingKey creates an MQ routing key
func (nm *NodeMessage) RoutingKey() string {
	dir := string(nm.Direction)
	typ := string(nm.Type)

	if nm.Type == SourceMessage {
		return dir + "." + typ + "." + nm.Source
	}
	if nm.Type == DeviceMessage {
		return dir + "." + typ + "." + nm.DeviceType
	}
	return dir + "." + typ
}
