module gitlab.com/sensorbucket/go-worker

go 1.15

require (
	github.com/sirupsen/logrus v1.8.1
	github.com/streadway/amqp v1.0.0
)
