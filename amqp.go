package worker

import (
	"log"
	"time"

	"github.com/streadway/amqp"
)

// ChanSetupFn is a function that is called when a new channel must be set up
type ChanSetupFn func(c *amqp.Channel) error

// AMQPSession ...
type AMQPSession struct {
	uri             string
	channel         *amqp.Channel
	conn            *amqp.Connection
	done            chan bool
	notifyConnClose chan *amqp.Error
	notifyChanClose chan *amqp.Error
	chanSetup       ChanSetupFn
}

// NewAMQPSession ...
func NewAMQPSession(uri string, chanSetup ChanSetupFn) *AMQPSession {
	s := AMQPSession{
		uri:       uri,
		done:      make(chan bool),
		chanSetup: chanSetup,
	}
	go s.connectionRescue()
	return &s
}

func (s *AMQPSession) connect() error {
	var err error

	s.conn, err = amqp.Dial(s.uri)
	if err != nil {
		return err
	}

	// Configure connection
	s.notifyConnClose = make(chan *amqp.Error)
	s.conn.NotifyClose(s.notifyConnClose)

	return nil
}

// Close ...
func (s *AMQPSession) Close() {
	close(s.done)
}

func (s *AMQPSession) connectionRescue() {
	// Initialize
	// Wait for event
	for {
		err := s.connect()
		if err != nil {
			log.Printf("AMQP error connecting to server: %s", err)
			select {
			// Stop the handler
			case <-s.done:
				break
				// Try again after 15 seconds
			case <-time.After(15 * time.Second):
				continue
			}
		}
		log.Printf("AMQP connection opened")

		// if done then we should close
		if done := s.channelRescue(); done {
			break
		}
		// Try again after 15 seconds
		<-time.After(15 * time.Second)
	}

	// Finish up
}

func (s *AMQPSession) channelRescue() bool {
	var err error

	for {
		// Configure channel
		s.channel, err = s.conn.Channel()
		if err != nil {
			log.Printf("AMQP error opening channel: %s", err)
			return false
		}

		// Setup channel
		if s.chanSetup != nil {
			err = s.chanSetup(s.channel)
			if err != nil {
				log.Printf("AMQP error initializing channel: %s", err)
				return false
			}
		}
		log.Printf("AMQP channel opened")

		s.notifyChanClose = make(chan *amqp.Error)
		s.channel.NotifyClose(s.notifyChanClose)

		select {
		// Should close; if s.done returns a value then the session is being closed
		// and we should stop
		case <-s.done:
			log.Println("AMQP session closing")
			return true
		// Connection lost; by returning false, the connectionRescue will loop
		// and open a new connection
		case err = <-s.notifyConnClose:
			log.Printf("AMQP connection lost: %s", err)
			return false
		// Channel lost; by continuing the loop will re-run and initialize a
		// new channel again
		case err = <-s.notifyChanClose:
			log.Printf("AMQP channel lost: %s", err)
			continue
		}
	}
}

// Channel returns the active channel
func (s *AMQPSession) Channel() *amqp.Channel {
	return s.channel
}
