package worker

import (
	"github.com/streadway/amqp"
	"gitlab.com/sensorbucket/go-worker/pipeline"
)

// ParseContext provides utility functions to the ETL script
type ParseContext struct {
	Message  *pipeline.NodeMessage
	worker   *Worker
	delivery amqp.Delivery
}

// FindDevice allows the worker to find a device based on properties and
// configuration settings. This device can then be bound to the node message
// Binding a NodeMessage to a Device must happen somewhere in the pipeline
func (ctx *ParseContext) FindDevice(filter *DeviceFilter) (*pipeline.Device, error) {
	return ctx.worker.FindDevice(filter, ctx.Message.Token)
}

// Publish published the node message back to the queue
func (ctx *ParseContext) Publish(msgTyp pipeline.MessageType, payload interface{}) error {
	nm := ctx.Message

	// Update payload
	nm.Payload = payload
	nm.Type = msgTyp

	return ctx.worker.Publish(nm)
}

// Finish parsing this message
func (ctx *ParseContext) Finish() error {
	return ctx.delivery.Ack(false)
}
