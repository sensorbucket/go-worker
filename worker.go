package worker

import (
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/sensorbucket/go-worker/pipeline"
)

// ETL ...
type ETL interface {
	Parse(ctx *ParseContext) error
}

// Worker ...
type Worker struct {
	log      *logrus.Logger
	mq       *AMQPSession
	queue    string
	exchange string
	mgmtURI  string
	etl      ETL
}

// Config ...
type Config struct {
	MGMTURI  string
	AMQPURI  string
	Queue    string
	Exchange string
	ETL      ETL
}

// Start ...
func Start(c *Config) {
	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, syscall.SIGHUP, syscall.SIGINT, syscall.SIGQUIT)

	w := &Worker{
		log:      logrus.New(),
		queue:    c.Queue,
		exchange: c.Exchange,
		mgmtURI:  c.MGMTURI,
		etl:      c.ETL,
	}
	w.mq = NewAMQPSession(c.AMQPURI, w.onNewChannel)

	<-sigChan
	w.log.Info("Shutting down")
	w.mq.Close()
}

func (w *Worker) onNewChannel(c *amqp.Channel) error {
	d, err := c.Consume(w.queue, "", false, false, false, false, nil)
	if err != nil {
		return err
	}

	go w.messageReceiver(d)

	return nil
}

func (w *Worker) messageReceiver(d <-chan amqp.Delivery) {
	w.log.Info("Handler started")
	for msg := range d {
		go w.messageHandler(msg)
	}
	w.log.Warn("Handler exited")
}

func (w *Worker) messageHandler(delivery amqp.Delivery) {
	// Unmarshal NodeMessage
	var nm pipeline.NodeMessage
	err := json.Unmarshal(delivery.Body, &nm)
	if err != nil {
		w.log.WithError(err).Error("Received message from queue, but could not unmarshal into Node Message")
		return
	}

	w.log.Infof("Parser start (id): %s", nm.ID)

	// Create parse context
	ctx := &ParseContext{
		Message:  &nm,
		worker:   w,
		delivery: delivery,
	}

	// Send to parser
	err = w.etl.Parse(ctx)
	if err != nil {
		w.log.Infof("Parser failed (id, err): %s, %s", nm.ID, err)
		delivery.Nack(false, false)
		return
	}
	w.log.Infof("Parser finished (id): %s", nm.ID)
}

// Publish publishes a node message to the queue
func (w *Worker) Publish(nm *pipeline.NodeMessage) error {

	// marshal to json string
	nmStr, err := json.Marshal(&nm)
	if err != nil {
		return fmt.Errorf("Failed to marshal parsed node message: %s", err)
	}

	// Send it to the exchange
	err = w.mq.channel.Publish(
		w.exchange,
		nm.RoutingKey(),
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        nmStr,
		},
	)
	if err != nil {
		return fmt.Errorf("Failed to send parsed node message to queue: %s", err)
	}

	return nil
}
